// Cleans the table so it shows only the correct data
const sweepTable = () => {
    $('#signups > tbody tr').not(':first').empty();
}

// Adds a signup row to the table
const addSignup = signup => {
  $('#signups > tbody:last-child').append(
    `<tr>
      <td>${signup.firstName}</td>
      <td>${signup.lastName}</td>
      <td>${signup.email}</td>
      <td>${signup.country}</td>
      <td>${signup.province}</td>
      <td>${signup.postalCode}</td>
      <td><button class="buttonDelete" id="${signup._id}">Delete</button></td>
    </tr>`
  );
};

// Shows the signups
const showSignups = async signupService => {
  // Find the latest 25 signups. They will come with the newest first
  const signups = await signupService.find({
    query: {
      $sort: { createdAt: -1 },
      $limit: 25
    }
  }).then(function(result) {
    // We want to show the newest signup last
        sweepTable();
        result.data.reverse().forEach(addSignup);
        $('.buttonDelete').on('click', function() {
            signupService.remove(this.id).then(function(deleteEntry) {
                showSignups(signupService);
            });
        });
   });

};

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  window.addEventListener('load', function() {
    grecaptcha.ready(function() {
      
        // Set up FeathersJS app
        var app = feathers();
        
        // Set up REST client
        var restClient = feathers.rest();

        // Configure an AJAX library with that client 
        app.configure(restClient.fetch(window.fetch));

        // Connect to the `signups` service
        const signups = app.service('signups');

        // Show existing signups in the table
        showSignups(signups);

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation')

        // Loop over them and prevent submission
        Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
            if (form.checkValidity()) {
                grecaptcha.execute('6Ld_MgEVAAAAALzJAG3Qns34qXk1JYbYwOSyKSJk', {action: 'signup'}).then(function(token) {
                    signups.create({
                        firstName: $('#firstName').val(),
                        lastName: $('#lastName').val(),
                        email: $('#email').val(),
                        country: $('#country').val(),
                        province: $('#province').val(),
                        postalCode: $('#postalCode').val(),
                        token: token
                    })
                    .then(function(result) {
                        $('#captchaMessage').addClass('d-none');
                        addSignup(result);
                        form.classList.remove('was-validated');
                        form.reset();
                    })
                    .catch(function() {
                        $('#captchaMessage').removeClass('d-none');
                    });                 
                });
            } else {
                form.classList.add('was-validated');
            }
            event.preventDefault();
            event.stopPropagation();
        }, false);
        })
    });
}, false);

}())
